//navbar//
const hamburger = document.querySelector(".hamburger");
const navMenu = document.querySelector(".nav-menu");
const navLink = document.querySelectorAll(".nav-link");

hamburger.addEventListener("click", mobileMenu);
navLink.forEach((n) => n.addEventListener("click", closeMenu));

function mobileMenu() {
  hamburger.classList.toggle("active");
  navMenu.classList.toggle("active");
}

function closeMenu() {
  hamburger.classList.remove("active");
  navMenu.classList.remove("active");
}

/*SHOP*/
const cardsContainer = document.querySelector(".container");

const addProductToCart = (event) => {
  //productsInCart object structure { productId: *quantity* }
  const productsInCart =
    JSON.parse(localStorage.getItem("productsInCart")) || {};

  const productId = event.target
    .closest(".card")
    .getAttribute("data-product-id");

  if (productsInCart[`${productId}`]) {
    productsInCart[`${productId}`]++;
  } else {
    productsInCart[`${productId}`] = 1;
  }

  localStorage.setItem("productsInCart", JSON.stringify(productsInCart));
};

const displayProducts = async () => {
  let products = await fetch("https://fakestoreapi.com/products").then((res) =>
    res.json()
  );

  products.forEach((product) => {
    const productCard = document.createElement("div");
    productCard.setAttribute("data-product-id", product.id);
    productCard.classList.add("card");

    const imageTitleWrapper = document.createElement("div");
    imageTitleWrapper.classList.add("image-title-wrapper");

    const productImage = document.createElement("img");
    productImage.src = product.image;
    imageTitleWrapper.appendChild(productImage);

    const productTitle = document.createElement("h3");
    productTitle.textContent = product.title;
    imageTitleWrapper.appendChild(productTitle);

    productCard.appendChild(imageTitleWrapper);

    const priceCartWrapper = document.createElement("div");
    priceCartWrapper.classList.add("price-cart-wrapper");

    const productDescription = document.createElement("p");
    productDescription.classList.add("product-description");
    productDescription.textContent = product.description;
    productCard.appendChild(productDescription);

    const productPrice = document.createElement("h4");
    productPrice.textContent = "$" + product.price;
    priceCartWrapper.appendChild(productPrice);

    const addProductToCartButton = document.createElement("button");
    addProductToCartButton.textContent = "Add To Cart";
    addProductToCartButton.addEventListener("click", addProductToCart);
    priceCartWrapper.appendChild(addProductToCartButton);

    productCard.appendChild(priceCartWrapper);

    cardsContainer.appendChild(productCard);
  });
};

displayProducts();


//-------------------Filter-------------------------//
$(document).ready(function () {
  // Funktion för att filtrera produkter baserat på data-product-id
  function filterProducts(category) {
    // Dölj alla element med attributet data-product-id
    $("[data-product-id]").hide();
    // Visa produkter baserat på den valda kategorin
    $(
      '[data-product-id="' + category.join('"], [data-product-id="') + '"]'
    ).show();
  }
  // Händelsehanterare för klick på navigationslänkar
  $(".nav-link").on("click", function (e) {
    e.preventDefault();
    // Hämta kategorin från texten i den klickade länken
    var category = $(this).text();
    // Definiera en koppling mellan kategorier och motsvarande data-product-id
    var categoryMapping = {
      "Mens Clothing": ["1", "2", "3", "4"],
      "Womens Clothing": ["15", "16", "17", "18", "19", "20"],
      Jewelry: ["5", "6", "7", "8"],
      Electronics: ["9", "10", "11", "12", "13", "14"],
    };
    // Filtrera produkter baserat på den valda kategorin
    filterProducts(categoryMapping[category]);
  });
});